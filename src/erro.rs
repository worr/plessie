use std::io;

use err_derive::Error;
use rusty_xinput::XInputLoadingFailure;
use vigem::VigemError;

pub type Result<T> = std::result::Result<T, PlessieError>;

#[derive(Debug, Error)]
pub enum PlessieError {
    #[error(display = "Could not to running vigem controller: {:?}", _0)]
    VigemConnectionError(#[error(source)] VigemError),
    #[error(display = "Could not load XInput library: {:?}", _0)]
    XInputLoadError(XInputLoadingFailure),
}

impl From<PlessieError> for io::Error {
    fn from(other: PlessieError) -> Self {
        io::Error::new(io::ErrorKind::Other, other)
    }
}

// this doesn't implement std::error::Error, so we can't use the #[error(source)]
// macro to populate this automatically.
impl From<XInputLoadingFailure> for PlessieError {
    fn from(other: XInputLoadingFailure) -> Self {
        PlessieError::XInputLoadError(other)
    }
}
