pub const BUTTONS_MASK: u32 = 0x0000ffff;
pub const BUTTONS_SHIFT: u32 = 0;
pub const LEFT_TRIGGER_MASK: u32 = 0xff000000;
pub const LEFT_TRIGGER_SHIFT: u32 = 24;
pub const RIGHT_TRIGGER_MASK: u32 = 0x00ff0000;
pub const RIGHT_TRIGGER_SHIFT: u32 = 16;

pub const LEFT_STICK_X_MASK: u64 = 0xffff000000000000;
pub const LEFT_STICK_X_SHIFT: u64 = 48;
pub const LEFT_STICK_Y_MASK: u64 = 0x0000ffff00000000;
pub const LEFT_STICK_Y_SHIFT: u64 = 32;
pub const RIGHT_STICK_X_MASK: u64 = 0x00000000ffff0000;
pub const RIGHT_STICK_X_SHIFT: u64 = 16;
pub const RIGHT_STICK_Y_MASK: u64 = 0x000000000000ffff;
pub const RIGHT_STICK_Y_SHIFT: u64 = 0;

#[inline]
pub fn i16_to_u64(num: i16) -> u64 {
    (num as u16) as u64
}
