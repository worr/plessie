use crate::bits;
use crate::types::{Gamepad, Point};

use std::fmt;
use std::sync::atomic::{AtomicBool, AtomicU32, AtomicU64, Ordering};
use std::sync::Arc;
use std::thread;
use std::time;

use rusty_xinput::{XInputHandle, XInputState};

pub struct ControllerState {
    connected: AtomicBool,

    buttons: AtomicU32,
    sticks: AtomicU64,
    last_packet: AtomicU32,
}

impl Default for ControllerState {
    fn default() -> Self {
        ControllerState {
            connected: AtomicBool::from(false),

            buttons: AtomicU32::from(0),
            sticks: AtomicU64::from(0),
            last_packet: AtomicU32::from(0),
        }
    }
}

impl ControllerState {
    // atomics look weird here, but they're a safe way
    // to do our bs thread updating without needing a mutable
    // reference to the controller state (note lack of `mut`)
    pub fn update_state(&self, input_state: &XInputState) {
        let raw = input_state.raw;
        let last_packet = self.last_packet.load(Ordering::Relaxed);
        if last_packet == raw.dwPacketNumber {
            return;
        }

        let sticks = (bits::i16_to_u64(raw.Gamepad.sThumbLX) << bits::LEFT_STICK_X_SHIFT)
            | (bits::i16_to_u64(raw.Gamepad.sThumbLY) << bits::LEFT_STICK_Y_SHIFT)
            | (bits::i16_to_u64(raw.Gamepad.sThumbRX) << bits::RIGHT_STICK_X_SHIFT)
            | (bits::i16_to_u64(raw.Gamepad.sThumbRY) << bits::RIGHT_STICK_Y_SHIFT);
        let buttons = ((raw.Gamepad.bLeftTrigger as u32) << bits::LEFT_TRIGGER_SHIFT)
            | ((raw.Gamepad.bRightTrigger as u32) << bits::RIGHT_TRIGGER_SHIFT)
            | ((raw.Gamepad.wButtons as u32) << bits::BUTTONS_SHIFT);
        self.buttons.store(buttons, Ordering::Relaxed);
        self.sticks.store(sticks, Ordering::Relaxed);
        self.last_packet
            .store(raw.dwPacketNumber, Ordering::Relaxed);
    }
}

impl Gamepad for ControllerState {
    fn face_buttons(&self) -> u16 {
        ((self.buttons.load(Ordering::Relaxed) & bits::BUTTONS_MASK) >> bits::BUTTONS_SHIFT) as u16
    }

    fn left_trigger(&self) -> u8 {
        ((self.buttons.load(Ordering::Relaxed) & bits::LEFT_TRIGGER_MASK)
            >> bits::LEFT_TRIGGER_SHIFT) as u8
    }

    fn right_trigger(&self) -> u8 {
        ((self.buttons.load(Ordering::Relaxed) & bits::RIGHT_TRIGGER_MASK)
            >> bits::RIGHT_TRIGGER_SHIFT) as u8
    }

    fn left_stick(&self) -> Point<i16> {
        Point {
            x: ((self.sticks.load(Ordering::Relaxed) & bits::LEFT_STICK_X_MASK)
                >> bits::LEFT_STICK_X_SHIFT) as i16,
            y: ((self.sticks.load(Ordering::Relaxed) & bits::LEFT_STICK_Y_MASK)
                >> bits::LEFT_STICK_Y_SHIFT) as i16,
        }
    }

    fn right_stick(&self) -> Point<i16> {
        Point {
            x: ((self.sticks.load(Ordering::Relaxed) & bits::RIGHT_STICK_X_MASK)
                >> bits::RIGHT_STICK_X_SHIFT) as i16,
            y: ((self.sticks.load(Ordering::Relaxed) & bits::RIGHT_STICK_Y_MASK)
                >> bits::RIGHT_STICK_Y_SHIFT) as i16,
        }
    }

    fn raw_buttons(&self) -> u32 {
        self.buttons.load(Ordering::Relaxed)
    }

    fn raw_sticks(&self) -> u64 {
        self.sticks.load(Ordering::Relaxed)
    }
}

impl fmt::Debug for ControllerState {
    // we implement this to lie about our bit-twiddly bullshit
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ControllerState")
            .field("face_buttons", &self.face_buttons())
            .field("left_trigger", &self.left_trigger())
            .field("right_trigger", &self.right_trigger())
            .field("left_stick", &self.left_stick())
            .field("right_stick", &self.right_stick())
            .field("raw_sticks", &format!("{:#016x}", &self.raw_sticks()))
            .field("raw_buttons", &format!("{:#08x}", &self.raw_buttons()))
            .finish()
    }
}

pub struct Controller {
    // don't make these pub
    pub id: u32,
    pub state: Arc<ControllerState>,
    handle: Arc<XInputHandle>,
}

impl Controller {
    pub fn new(id: u32, handle: Arc<XInputHandle>) -> Self {
        Controller {
            id,
            state: Arc::new(ControllerState::default()),
            handle,
        }
    }

    pub fn run(&self) {
        loop {
            if self.handle.get_state(self.id).is_ok() {
                println!("Controller {} connected", self.id);
                self.state.connected.store(true, Ordering::Relaxed);
                loop {
                    match self.handle.get_state(self.id) {
                        Ok(state) => {
                            self.state.update_state(&state);
                            //println!("controller {} state: {:?}", self.id, self.state);
                        }
                        Err(_) => {
                            println!("Controller {} disconnected", self.id);
                            self.state.connected.store(true, Ordering::Relaxed);
                            break;
                        }
                    }

                    // we can poll more often, but that fucks CPU
                    thread::sleep(time::Duration::from_micros(1))
                }
            }

            // poll for connections
            thread::sleep(time::Duration::from_secs(10));
        }
    }

    pub fn connected(&self) -> bool {
        self.state.connected.load(Ordering::Relaxed)
    }
}
