use crate::bits;
use crate::input::Controller;
use crate::output::PlessieControllerState;
use crate::types::{Gamepad, Point};

use std::sync::Arc;

pub fn full_consensus(controllers: &[Arc<Controller>]) -> PlessieControllerState {
    let mut staging = PlessieControllerState::default();

    let mut left_stick_accum: Point<i64> = Point::default();
    let mut right_stick_accum: Point<i64> = Point::default();

    let mut left_trigger_accum: u32 = 0;
    let mut right_trigger_accum: u32 = 0;

    let mut face_buttons = 0xffff;

    let connected = controllers.len() as i64;

    for controller in controllers {
        let new_state = PlessieControllerState::from(&*controller.state);
        face_buttons &= new_state.face_buttons();

        left_stick_accum += new_state.left_stick().into();
        right_stick_accum += new_state.right_stick().into();

        left_trigger_accum += new_state.left_trigger() as u32;
        right_trigger_accum += new_state.right_trigger() as u32;
    }

    staging.set_all_sticks(
        Point {
            x: bits::i16_to_u64((left_stick_accum.x / connected) as i16),
            y: bits::i16_to_u64((left_stick_accum.y / connected) as i16),
        },
        Point {
            x: bits::i16_to_u64((right_stick_accum.x / connected) as i16),
            y: bits::i16_to_u64((right_stick_accum.y / connected) as i16),
        },
    );

    staging.set_all_buttons(
        left_trigger_accum / connected as u32,
        right_trigger_accum / connected as u32,
        face_buttons as u32,
    );

    //println!("staging ({}): {:?}", connected, staging);

    staging
}
