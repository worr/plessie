use crate::bits;
use crate::erro::Result;
use crate::input::{Controller, ControllerState};
use crate::types::{ConsensusFn, Gamepad, Point};

use std::fmt;
use std::sync::Arc;
use std::thread;
use std::time;

use vigem::{self, Vigem, XButton, XUSBReport};

pub struct PlessieControllerState {
    buttons: u32,
    sticks: u64,
}

impl Default for PlessieControllerState {
    fn default() -> Self {
        PlessieControllerState {
            buttons: 0,
            sticks: 0,
        }
    }
}

impl From<&ControllerState> for PlessieControllerState {
    fn from(other: &ControllerState) -> Self {
        PlessieControllerState {
            buttons: other.raw_buttons(),
            sticks: other.raw_sticks(),
        }
    }
}

impl Gamepad for PlessieControllerState {
    fn face_buttons(&self) -> u16 {
        ((self.buttons & bits::BUTTONS_MASK) >> bits::BUTTONS_SHIFT) as u16
    }

    fn left_trigger(&self) -> u8 {
        ((self.buttons & bits::LEFT_TRIGGER_MASK) >> bits::LEFT_TRIGGER_SHIFT) as u8
    }

    fn right_trigger(&self) -> u8 {
        ((self.buttons & bits::RIGHT_TRIGGER_MASK) >> bits::RIGHT_TRIGGER_SHIFT) as u8
    }

    fn left_stick(&self) -> Point<i16> {
        Point {
            x: ((self.sticks & bits::LEFT_STICK_X_MASK) >> bits::LEFT_STICK_X_SHIFT) as i16,
            y: ((self.sticks & bits::LEFT_STICK_Y_MASK) >> bits::LEFT_STICK_Y_SHIFT) as i16,
        }
    }

    fn right_stick(&self) -> Point<i16> {
        Point {
            x: ((self.sticks & bits::RIGHT_STICK_X_MASK) >> bits::RIGHT_STICK_X_SHIFT) as i16,
            y: ((self.sticks & bits::RIGHT_STICK_Y_MASK) >> bits::RIGHT_STICK_Y_SHIFT) as i16,
        }
    }

    fn raw_buttons(&self) -> u32 {
        self.buttons
    }

    fn raw_sticks(&self) -> u64 {
        self.sticks
    }
}

impl fmt::Debug for PlessieControllerState {
    // we implement this to lie about our bit-twiddly bullshit
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ControllerState")
            .field("face_buttons", &self.face_buttons())
            .field("left_trigger", &self.left_trigger())
            .field("right_trigger", &self.right_trigger())
            .field("left_stick", &self.left_stick())
            .field("right_stick", &self.right_stick())
            .field("raw_sticks", &format!("{:#016x}", &self.raw_sticks()))
            .field("raw_buttons", &format!("{:#08x}", &self.raw_buttons()))
            .finish()
    }
}

impl PlessieControllerState {
    pub fn set_all_buttons(&mut self, left_trigger: u32, right_trigger: u32, face_buttons: u32) {
        self.buttons = left_trigger << bits::LEFT_TRIGGER_SHIFT
            | right_trigger << bits::RIGHT_TRIGGER_SHIFT
            | face_buttons << bits::BUTTONS_SHIFT;
    }

    pub fn set_all_sticks(&mut self, left_stick: Point<u64>, right_stick: Point<u64>) {
        self.sticks = left_stick.x << bits::LEFT_STICK_X_SHIFT
            | left_stick.y << bits::LEFT_STICK_Y_SHIFT
            | right_stick.x << bits::RIGHT_STICK_X_SHIFT
            | right_stick.y << bits::RIGHT_STICK_Y_SHIFT;
    }
}

pub struct PlessieController {
    handle: Vigem,
    target: vigem::Target,
    state: PlessieControllerState,
    pub controller_number: Option<u32>,
}

impl PlessieController {
    pub fn new() -> Self {
        PlessieController {
            handle: Vigem::new(),
            target: vigem::Target::new(vigem::TargetType::Xbox360),
            state: PlessieControllerState::default(),
            controller_number: None,
        }
    }

    pub fn connect(&mut self) -> Result<()> {
        self.handle.connect()?;
        self.handle.target_add(&mut self.target)?;
        self.controller_number = Some(self.handle.xbox_get_user_index(&self.target));
        Ok(())
    }

    pub fn update_state(&mut self, new_state: PlessieControllerState) -> Result<()> {
        self.state = new_state;

        let report = XUSBReport {
            w_buttons: XButton::from_bits(self.state.face_buttons()).unwrap_or_else(XButton::empty),
            b_left_trigger: self.state.left_trigger(),
            b_right_trigger: self.state.right_trigger(),
            s_thumb_lx: self.state.left_stick().x,
            s_thumb_ly: self.state.left_stick().y,
            s_thumb_rx: self.state.right_stick().x,
            s_thumb_ry: self.state.right_stick().y,
        };

        self.handle.update(&self.target, &report)?;
        Ok(())
    }

    pub fn start(&mut self, controllers: Vec<Arc<Controller>>, func: ConsensusFn) -> Result<()> {
        loop {
            let controllers: Vec<Arc<Controller>> = controllers
                .iter()
                .filter(|c| c.connected())
                .cloned()
                .collect();

            if !controllers.is_empty() {
                let state = func(&controllers);
                self.update_state(state)?;
            }

            thread::sleep(time::Duration::from_micros(1));
        }
    }
}
