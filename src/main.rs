use crate::erro::Result;

use std::io;
use std::result;
use std::sync::Arc;
use std::thread;

use rusty_xinput::XInputHandle;

mod bits;
mod erro;
mod input;
mod output;
mod scheme;
mod types;

const MAX_CONTROLLERS: u32 = 4;

fn init_plessie() -> Result<output::PlessieController> {
    let mut p = output::PlessieController::new();
    p.connect()?;
    Ok(p)
}

fn init_xinput() -> Result<XInputHandle> {
    Ok(XInputHandle::load_default()?)
}

fn main() -> result::Result<(), io::Error> {
    let handle = Arc::new(init_xinput()?);
    let mut controllers = Vec::new();
    let mut plessie = init_plessie()?;

    for id in 0..MAX_CONTROLLERS {
        // poison plessie controller number
        if id == plessie.controller_number.unwrap() {
            continue;
        }

        let thandle = handle.clone();
        let controller = Arc::new(input::Controller::new(id, thandle));
        let c = controller.clone();

        thread::spawn(move || c.run());
        controllers.push(controller);
    }

    plessie.start(controllers, scheme::full_consensus)?;
    Ok(())
}
