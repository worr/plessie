use crate::input::Controller;
use crate::output::PlessieControllerState;

use std::convert::From;
use std::ops::{Add, AddAssign};
use std::sync::Arc;

pub trait Gamepad {
    fn face_buttons(&self) -> u16;

    fn left_trigger(&self) -> u8;
    fn right_trigger(&self) -> u8;

    fn left_stick(&self) -> Point<i16>;
    fn right_stick(&self) -> Point<i16>;

    fn raw_buttons(&self) -> u32;
    fn raw_sticks(&self) -> u64;
}

pub type ConsensusFn = fn(&[Arc<Controller>]) -> PlessieControllerState;

#[derive(Clone, Copy, Debug)]
pub struct Point<T: Add> {
    pub x: T,
    pub y: T,
}

impl<T: Add<Output = T>> Add for Point<T> {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<T: Add<Output = T> + Copy> AddAssign for Point<T> {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x + other.x,
            y: self.y + other.y,
        };
    }
}

// special case for accumulators
impl From<Point<i16>> for Point<i64> {
    fn from(src: Point<i16>) -> Point<i64> {
        Point {
            x: src.x as i64,
            y: src.y as i64,
        }
    }
}

impl Default for Point<i16> {
    fn default() -> Self {
        Point { x: 0, y: 0 }
    }
}

impl Default for Point<i64> {
    fn default() -> Self {
        Point { x: 0, y: 0 }
    }
}
